import {System} from '../../node_modules/ecsy/build/ecsy.module.js';
import {ComposantFaim} from '../components/ComposantFaim.js';

export class SystemFaim extends System {
    init() {

    }
    execute(delta, time) {
        this.queries.faim.results.forEach(entity => {
            let faim = entity.getMutableComponent(ComposantFaim);
            faim.value = faim.value - 1;
            console.log(faim);
        })
    }
}

SystemFaim.queries = {
    faim: { components: [ ComposantFaim ]}
}