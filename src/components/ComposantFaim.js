import { Component, Types } from '../../node_modules/ecsy/build/ecsy.module.js';

export class ComposantFaim extends Component {}

ComposantFaim.schema = {
    value: { type: Types.Number, default: 100 }
}