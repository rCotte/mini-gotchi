import { World, System, Component, TagComponent, Types } from "./node_modules/ecsy/build/ecsy.module.js";

import { SystemFaim } from './src/systems/SystemFaim.js';

import { ComposantFaim } from './src/components/ComposantFaim.js';



const world = new World();
world.registerComponent(ComposantFaim);
world.registerSystem(SystemFaim);

let miniGotchi = world.createEntity();
miniGotchi.addComponent(ComposantFaim);

document.querySelector('button').addEventListener('click', () => {
    world.execute();
})